package com.duc.filestore.aws;

import software.amazon.awssdk.regions.Region;

public interface S3Configuration {

    String getAccessKey();

    String getSecretKey();

    Region getRegion();

}
