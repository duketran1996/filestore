package com.duc.filestore.aws;

import com.duc.filestore.repository.FileRepository;
import com.duc.filestore.repository.FileStoreException;
import com.duc.filestore.repository.FileTransactionStatus;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.CreateBucketConfiguration;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.HeadBucketRequest;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.S3Object;
import software.amazon.awssdk.services.s3.presigner.S3Presigner;
import software.amazon.awssdk.services.s3.presigner.model.GetObjectPresignRequest;
import software.amazon.awssdk.services.s3.presigner.model.PresignedGetObjectRequest;
import software.amazon.awssdk.services.s3.presigner.model.PresignedPutObjectRequest;
import software.amazon.awssdk.services.s3.presigner.model.PutObjectPresignRequest;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.time.Duration;

public class S3Repository implements FileRepository {

	private S3Client s3Client;
	private S3Presigner s3Presigner;

	public S3Repository(S3Configuration s3Configuration) {
		StaticCredentialsProvider credentials = StaticCredentialsProvider
				.create(AwsBasicCredentials.create(s3Configuration.getAccessKey(), s3Configuration.getSecretKey()));
		s3Client = S3Client.builder().region(s3Configuration.getRegion()).credentialsProvider(credentials).build();
		s3Presigner = S3Presigner.builder().region(s3Configuration.getRegion()).credentialsProvider(credentials)
				.build();
	}

	@Override
	public void createBucket(String bucketName) throws FileStoreException {
		CreateBucketRequest createBucketRequest = CreateBucketRequest.builder().bucket(bucketName)
				.createBucketConfiguration(CreateBucketConfiguration.builder().build()).build();

		try {
			s3Client.createBucket(createBucketRequest);
		} catch (Exception e) {
			throw new FileStoreException(String.format("AWS-S3 failed to create bucket with name \"%s\"", bucketName),
					e.getCause());
		}
	}

	@Override
	public void deleteBucket(String bucketName) throws FileStoreException {
		ListObjectsV2Request listObjectsV2Request = ListObjectsV2Request.builder().bucket(bucketName).build();
		ListObjectsV2Response listObjectsV2Response;

		do {
			listObjectsV2Response = s3Client.listObjectsV2(listObjectsV2Request);
			for (S3Object s3Object : listObjectsV2Response.contents()) {
				try {
					s3Client.deleteObject(DeleteObjectRequest.builder().bucket(bucketName).key(s3Object.key()).build());
				} catch (Exception e) {
					throw new FileStoreException(String.format("AWS-S3 failed to delete object \"%s\" in bucket \"%s\"",
							s3Object.key(), bucketName), e.getCause());
				}
			}

			listObjectsV2Request = ListObjectsV2Request.builder().bucket(bucketName)
					.continuationToken(listObjectsV2Response.nextContinuationToken()).build();

		} while (listObjectsV2Response.isTruncated());

		DeleteBucketRequest deleteBucketRequest = DeleteBucketRequest.builder().bucket(bucketName).build();

		try {
			s3Client.deleteBucket(deleteBucketRequest);
		} catch (Exception e) {
			throw new FileStoreException(String.format("AWS-S3 failed to delete bucket with name \"%s\"", bucketName),
					e.getCause());
		}
	}

	@Override
	public boolean doesBucketExist(String bucketName) throws FileStoreException {
		HeadBucketRequest headBucketRequest = HeadBucketRequest.builder().bucket(bucketName).build();

		try {
			s3Client.headBucket(headBucketRequest);
		} catch (Exception e) {
			throw new FileStoreException(
					String.format("AWS-S3 failed to check bucket exist or bucket does not exist with name \"%s\"",
							bucketName),
					e.getCause());
		}
		return true;
	}

	@Override
	public FileTransactionStatus uploadDocument(String bucketName, String objectKey, File file)
			throws FileStoreException {
		PutObjectRequest putObjectRequest = PutObjectRequest.builder().bucket(bucketName).key(objectKey).build();
		try {
			s3Client.putObject(putObjectRequest, RequestBody.fromFile(file));
		} catch (Exception e) {
			throw new FileStoreException(
					String.format("AWS-S3 failed to upload object as file \"%s\" with key \"%s\" to bucket \"%s\"",
							file.getName(), objectKey, bucketName),
					e.getCause());
		}
		return FileTransactionStatus.UPLOADED;
	}

	@Override
	public FileTransactionStatus uploadDocument(String bucketName, String objectKey, InputStream inputStream)
			throws FileStoreException {
		PutObjectRequest putObjectRequest = PutObjectRequest.builder().bucket(bucketName).key(objectKey).build();
		try {
			s3Client.putObject(putObjectRequest, RequestBody.fromInputStream(inputStream, inputStream.available()));
		} catch (Exception e) {
			throw new FileStoreException(
					String.format("AWS-S3 failed to upload object as input stream with key \"%s\" to bucket \"%s\"",
							objectKey, bucketName),
					e.getCause());
		}
		return FileTransactionStatus.UPLOADED;
	}

	@Override
	public FileTransactionStatus deleteDocument(String bucketName, String objectKey) throws FileStoreException {
		DeleteObjectRequest deleteObjectRequest = DeleteObjectRequest.builder().bucket(bucketName).key(objectKey)
				.build();
		try {
			s3Client.deleteObject(deleteObjectRequest);
		} catch (Exception e) {
			throw new FileStoreException(String
					.format("AWS-S3 failed to delete object with key \"%s\" from bucket \"%s\"", objectKey, bucketName),
					e.getCause());
		}
		return FileTransactionStatus.DELETED;
	}

	@Override
	public InputStream downloadDocument(String bucketName, String objectKey) throws FileStoreException {
		GetObjectRequest getObjectRequest = GetObjectRequest.builder().bucket(bucketName).key(objectKey).build();
		InputStream object;
		try {
			object = s3Client.getObject(getObjectRequest);
		} catch (Exception e) {
			throw new FileStoreException(String.format("AWS-S3 failed to get object with key \"%s\" from bucket \"%s\"",
					objectKey, bucketName), e.getCause());
		}
		return object;
	}
	
	@Override
	public String generatePreAuthenticatenUploadUrl(String bucketName, String objectKey, Duration duration)
			throws FileStoreException {
		try {
			PutObjectRequest putObjectRequest = PutObjectRequest.builder().bucket(bucketName).key(objectKey).build();
			PutObjectPresignRequest putObjectPresignRequest = PutObjectPresignRequest.builder()
					.signatureDuration(duration).putObjectRequest(putObjectRequest).build();
			PresignedPutObjectRequest presignedPutObjectRequest = s3Presigner.presignPutObject(putObjectPresignRequest);
			return presignedPutObjectRequest.url().toString();
		} catch (Exception e) {
			throw new FileStoreException(String.format(
					"AWS-S3 failed to generate pre-authentication upload url with object key \"%s\" from bucket \"%s\" and duration time \"%s\"",
					objectKey, bucketName, duration.toString()), e.getCause());
		}
	}

	@Override
	public String generatePreAuthenticatenDownloadUrl(String bucketName, String objectKey, Duration duration)
			throws FileStoreException {
		try {
			GetObjectRequest getObjectRequest = GetObjectRequest.builder().bucket(bucketName).key(objectKey).build();
			GetObjectPresignRequest getObjectPresignRequest = GetObjectPresignRequest.builder()
					.signatureDuration(duration).getObjectRequest(getObjectRequest).build();
			PresignedGetObjectRequest presignedGetObjectRequest = s3Presigner.presignGetObject(getObjectPresignRequest);
			return presignedGetObjectRequest.url().toString();
		} catch (Exception e) {
			throw new FileStoreException(String.format(
					"AWS-S3 failed to generate pre-authentication download url with object key \"%s\" from bucket \"%s\" and duration time \"%s\"",
					objectKey, bucketName, duration.toString()), e.getCause());
		}
	}

}
