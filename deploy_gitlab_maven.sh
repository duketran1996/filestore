#!/bin/sh

gitlab_url=https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/packages/maven
gitlab_repo=gitlab-maven
gitlab_version=v1.0
gitlab_groupId="com.filestore"
file_settings=ci_settings.xml

mvn deploy:deploy-file -Durl=$gitlab_url -DrepositoryId=$gitlab_repo -Dfile=aws/target/aws-v1.0.jar -DgroupId=$gitlab_groupId  -DartifactId="aws" -Dversion=$gitlab_version -Dpackaging=jar -DgeneratePom=true -s $file_settings
mvn deploy:deploy-file -Durl=$gitlab_url -DrepositoryId=$gitlab_repo -Dfile=oracle/target/oracle-v1.0.jar -DgroupId=$gitlab_groupId  -DartifactId="oracle" -Dversion=$gitlab_version -Dpackaging=jar -DgeneratePom=true -s $file_settings
