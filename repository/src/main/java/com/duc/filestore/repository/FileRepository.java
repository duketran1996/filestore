package com.duc.filestore.repository;

import java.io.File;
import java.io.InputStream;
import java.time.Duration;

public interface FileRepository {

	void createBucket(String bucketName) throws FileStoreException;

	void deleteBucket(String bucketName) throws FileStoreException;

	boolean doesBucketExist(String bucketName) throws FileStoreException;

	FileTransactionStatus uploadDocument(String bucketName, String objectKey, File file) throws FileStoreException;

	FileTransactionStatus uploadDocument(String bucketName, String objectKey, InputStream inputStream)
			throws FileStoreException;

	FileTransactionStatus deleteDocument(String bucketName, String objectKey) throws FileStoreException;

	InputStream downloadDocument(String bucketName, String objectKey) throws FileStoreException;

	String generatePreAuthenticatenUploadUrl(String bucketName, String objectKey, Duration duration)
			throws FileStoreException;

	String generatePreAuthenticatenDownloadUrl(String bucketName, String objectKey, Duration duration)
			throws FileStoreException;
}
