# Filestore

Filestore is a java library wrapper built to interact with cloud file storage API for AWS S3 and Oracle Cloud Infrastructure.

## Installation

Include the dependency in your [maven project](https://maven.apache.org/) (pom.xml) to install filestore.

### AWS S3

#### Maven XML:
```xml
<dependency>
  <groupId>com.filestore</groupId>
  <artifactId>aws</artifactId>
  <version>v1.0</version>
</dependency>
```

#### Maven Command:

```bash
mvn dependency:get -Dartifact=com.filestore:aws:v1.0
```

### Oracle Cloud Infrastructure

#### Maven XML:
```xml
<dependency>
  <groupId>com.filestore</groupId>
  <artifactId>oracle</artifactId>
  <version>v1.0</version>
</dependency>
```

#### Maven Command:

```bash
mvn dependency:get -Dartifact=com.filestore:oracle:v1.0
```

## Usage

Config and set your AWS S3 credentials:

```java
private static S3Repository s3Repo;

public static void setup() throws IOException {

		S3Configuration s3Configuration = new S3Configuration() {

			@Override
			public String getAccessKey() {
				return System.getenv(ACCESS_KEY);
			}

			@Override
			public String getSecretKey() {
				return System.getenv(SECRET_KEY);
			}

			@Override
			public Region getRegion() {
				return Region.of(System.getenv(REGION));
			}
		};

		s3Repo = new S3Repository(s3Configuration);
}
```

Config and set your Oracle Cloud Infrastructure credentials:
```java
private static ObjectStoreRepository objectStoreRepo;

public static void setup() throws IOException {

		ObjectStoreConfiguration objectStoreConfiguration = new ObjectStoreConfiguration() {

			@Override
			public String getTenantId() {
				return System.getenv(TENANT_ID);
			}

			@Override
			public String getUserId() {
				return System.getenv(USER_ID);
			}

			@Override
			public String getFingerPrint() {
				return System.getenv(FINGERPRINT);
			}

			@Override
			public String getPassphrase() {
				return System.getenv(PASSPHRASE);
			}

			@Override
			public String privateKey() {
				try {
					return getKey(System.getenv(PRIVATE_KEY));
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			public Region getRegion() {
				return Region.fromRegionId(System.getenv(REGION));
			}

			@Override
			public String getCompartmentId() {
				return System.getenv(COMPARTMENT_ID);
			}

			@Override
			public String getNameSpace() {
				return System.getenv(NAMESPACE);
			}

			@Override
			public String getBaseUrl() {
				return String.format("https://objectstorage.%s.oraclecloud.com", System.getenv(REGION));
			}

		};

		objectStoreRepo = new ObjectStoreRepository(objectStoreConfiguration);
}

```


Start using methods provide from your objects S3Repository or ObjectStoreRepository. The provide features are:

```java
void createBucket(String bucketName);

void deleteBucket(String bucketName);

boolean doesBucketExist(String bucketName);

FileTransactionStatus uploadDocument(String bucketName, String objectKey, File file);

FileTransactionStatus uploadDocument(String bucketName, String objectKey, InputStream inputStream);

FileTransactionStatus deleteDocument(String bucketName, String objectKey);

InputStream downloadDocument(String bucketName, String objectKey);

String generatePreAuthenticatenUploadUrl(String bucketName, String objectKey, Duration duration);

String generatePreAuthenticatenDownloadUrl(String bucketName, String objectKey, Duration duration);

```

## Note
Please refer to the test folder for more details usage.

## Roadmap
_Provide support for Microsoft Azure.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
