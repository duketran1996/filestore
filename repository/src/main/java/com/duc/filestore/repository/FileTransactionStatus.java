package com.duc.filestore.repository;

public enum FileTransactionStatus {
	UPLOADED,
	DELETED;
}
