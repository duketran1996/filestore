package com.duc.filestore.oci.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.io.TempDir;

import com.duc.filestore.oci.ObjectStoreConfiguration;
import com.duc.filestore.oci.ObjectStoreRepository;
import com.duc.filestore.repository.FileStoreException;
import com.duc.filestore.repository.FileTransactionStatus;
import com.oracle.bmc.Region;

@TestMethodOrder(OrderAnnotation.class)
public class ObjectStoreRepositoryTest {

	private static ObjectStoreRepository objectStoreRepo;
	private final static String TENANT_ID = "tenancy";
	private final static String USER_ID = "user";
	private final static String FINGERPRINT = "fingerprint";
	private final static String PRIVATE_KEY = "oci_api_key_file";
	private final static String PASSPHRASE = "passphrase";
	private final static String REGION = "oci_region";
	private final static String COMPARTMENT_ID = "compartment";
	private final static String NAMESPACE = "namespace";
	private final static String BUCKET_NAME = "ducbucket1";
	private final static String OBJECT_KEY = "1";
	private final static String OBJECT_KEY_STREAM = "2";
	private final static String OBJECT_KEY_PREAUTH = "3";
	private final static String UPLOAD_FILE_NAME = "uploadFile.txt";
	private final static String MESSAGE_PREAUTH = "This is presigned URL test";

	private static String getKey(String filename) throws IOException {
		// Read key from file
		String strKeyPEM = "";
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String line;
		while ((line = br.readLine()) != null) {
			strKeyPEM += line + "\n";
		}
		br.close();
		return strKeyPEM;
	}

	@TempDir
	public static Path tempDir;
	private static File tempFile;

	@BeforeAll
	public static void setup() throws IOException {

		ObjectStoreConfiguration objectStoreConfiguration = new ObjectStoreConfiguration() {

			@Override
			public String getTenantId() {
				return System.getenv(TENANT_ID);
			}

			@Override
			public String getUserId() {
				return System.getenv(USER_ID);
			}

			@Override
			public String getFingerPrint() {
				return System.getenv(FINGERPRINT);
			}

			@Override
			public String getPassphrase() {
				return System.getenv(PASSPHRASE);
			}

			@Override
			public String privateKey() {
				try {
					return getKey(System.getenv(PRIVATE_KEY));
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			public Region getRegion() {
				return Region.fromRegionId(System.getenv(REGION));
			}

			@Override
			public String getCompartmentId() {
				return System.getenv(COMPARTMENT_ID);
			}

			@Override
			public String getNameSpace() {
				return System.getenv(NAMESPACE);
			}

			@Override
			public String getBaseUrl() {
				return String.format("https://objectstorage.%s.oraclecloud.com", System.getenv(REGION));
			}

		};

		objectStoreRepo = new ObjectStoreRepository(objectStoreConfiguration);

		tempFile = Files.createFile(tempDir.resolve(UPLOAD_FILE_NAME)).toFile();
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
		writer.write("This is upload object test");
		writer.close();
	}

	@Test
	@Order(1)
	public void createBucketTest() {
		try {
			objectStoreRepo.createBucket(BUCKET_NAME);
		} catch (FileStoreException e) {
			fail("Create bucket test failed: " + e.getMessage());
		}
	}

	@Test
	@Order(2)
	public void existBucketAfterCreateTest() {
		try {
			boolean bucketExist = objectStoreRepo.doesBucketExist(BUCKET_NAME);
			assertEquals(true, bucketExist, String.format("Does bucket exist boolean value %s", bucketExist));
		} catch (FileStoreException e) {
			fail("Does bucket exist after create test failed: " + e.getMessage());
		}
	}

	@Test
	@Order(3)
	public void objectFileUploadTest() {
		try {
			FileTransactionStatus status = objectStoreRepo.uploadDocument(BUCKET_NAME, OBJECT_KEY, tempFile);
			assertEquals(FileTransactionStatus.UPLOADED, status,
					String.format("File transaction status actual %s", status));
		} catch (FileStoreException e) {
			fail("Upload object as file to bucket test failed: " + e.getMessage());
		}
	}

	@Test
	@Order(4)
	public void objectInputStreamUploadTest() {
		try {
			FileInputStream inputStream = new FileInputStream(tempFile);
			FileTransactionStatus status = objectStoreRepo.uploadDocument(BUCKET_NAME, OBJECT_KEY_STREAM, inputStream);
			assertEquals(FileTransactionStatus.UPLOADED, status,
					String.format("File transaction status actual %s", status));
		} catch (IOException e) {
			fail("Failed to read file as input stream: " + e.getMessage());
		} catch (FileStoreException e) {
			fail("Upload object as input stream to bucket test failed: " + e.getMessage());
		}
	}

	@Test
	@Order(5)
	public void objectDownloadTest() {
		try {
			InputStream file = new FileInputStream(tempFile);
			InputStream object = objectStoreRepo.downloadDocument(BUCKET_NAME, OBJECT_KEY);
			assertNotNull(object, "Object stream is null");
			assertTrue(IOUtils.contentEquals(object, file), "File contents are not equal");
		} catch (IOException e) {
			fail("Failed to read file as input stream: " + e.getMessage());
		} catch (FileStoreException e) {
			fail("Download object test failed: " + e.getMessage());
		}
	}

	@Test
	@Order(6)
	public void objectDeleteTest() {
		try {
			FileTransactionStatus status = objectStoreRepo.deleteDocument(BUCKET_NAME, OBJECT_KEY);
			assertEquals(FileTransactionStatus.DELETED, status,
					String.format("File transaction status actual %s", status));
		} catch (FileStoreException e) {
			fail("Delete object from bucket test failed: " + e.getMessage());
		}
	}

	@Test
	@Order(7)
	public void objectPreAuthenticateUploadUrlTest() {
		try {

			String url = objectStoreRepo.generatePreAuthenticatenUploadUrl(BUCKET_NAME, OBJECT_KEY_PREAUTH,
					Duration.ofMinutes(5));
			assertNotNull(url, "Presigned upload url is null");

			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "text/plain");
			connection.setRequestMethod("PUT");

			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(MESSAGE_PREAUTH);
			out.close();
			assertEquals(200, connection.getResponseCode(), "Failed to upload object with presigned url");

			InputStream object = objectStoreRepo.downloadDocument(BUCKET_NAME, OBJECT_KEY_PREAUTH);
			InputStream messageStream = new ByteArrayInputStream(MESSAGE_PREAUTH.getBytes(StandardCharsets.UTF_8));
			assertTrue(IOUtils.contentEquals(object, messageStream), "File contents are not equal");

		} catch (IOException e) {
			fail("Url failed to open connection: " + e.getMessage());
		} catch (FileStoreException e) {
			fail("Generate presigned url for object upload test failed: " + e.getMessage());
		}
	}

	@Test
	@Order(8)
	public void objectPreAuthenticateDownloadUrlTest() {

		try {
			String url = objectStoreRepo.generatePreAuthenticatenDownloadUrl(BUCKET_NAME, OBJECT_KEY_PREAUTH,
					Duration.ofMinutes(5));
			assertNotNull(url, "Presigned download url is null");

			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();

			InputStream object = connection.getInputStream();
			InputStream messageStream = new ByteArrayInputStream(MESSAGE_PREAUTH.getBytes(StandardCharsets.UTF_8));
			assertTrue(IOUtils.contentEquals(object, messageStream), "File contents are not equal");

		} catch (IOException e) {
			fail("Url failed to open connection: " + e.getMessage());
		} catch (FileStoreException e) {
			fail("Generate presigned url for object download test failed: " + e.getMessage());
		}
	}

	@Test
	@Order(9)
	public void deleteBucketTest() {
		try {
			objectStoreRepo.deleteBucket(BUCKET_NAME);
		} catch (FileStoreException e) {
			fail("Delete bucket test failed: " + e.getMessage());
		}
	}

	@Test()
	@Order(10)
	public void existBucketAfterDeleteTest() {
		assertThrows(FileStoreException.class, () -> {
			objectStoreRepo.doesBucketExist(BUCKET_NAME);
		});

	}
}
