package com.duc.filestore.repository;

public class FileStoreException extends Exception {

	private static final long serialVersionUID = 5761925487298103851L;

	public FileStoreException() {
		super();
	}

	public FileStoreException(String message) {
		super(message);
	}

	public FileStoreException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
