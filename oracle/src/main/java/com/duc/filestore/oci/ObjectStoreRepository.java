package com.duc.filestore.oci;

import com.duc.filestore.repository.FileRepository;
import com.duc.filestore.repository.FileStoreException;
import com.duc.filestore.repository.FileTransactionStatus;
import com.oracle.bmc.auth.SimpleAuthenticationDetailsProvider;
import com.oracle.bmc.objectstorage.ObjectStorage;
import com.oracle.bmc.objectstorage.ObjectStorageClient;
import com.google.common.base.Supplier;
import com.oracle.bmc.objectstorage.model.CreateBucketDetails;
import com.oracle.bmc.objectstorage.model.CreatePreauthenticatedRequestDetails;
import com.oracle.bmc.objectstorage.model.CreatePreauthenticatedRequestDetails.AccessType;
import com.oracle.bmc.objectstorage.requests.CreateBucketRequest;
import com.oracle.bmc.objectstorage.requests.CreatePreauthenticatedRequestRequest;
import com.oracle.bmc.objectstorage.requests.DeleteBucketRequest;
import com.oracle.bmc.objectstorage.requests.DeleteObjectRequest;
import com.oracle.bmc.objectstorage.requests.DeletePreauthenticatedRequestRequest;
import com.oracle.bmc.objectstorage.requests.GetObjectRequest;
import com.oracle.bmc.objectstorage.requests.HeadBucketRequest;
import com.oracle.bmc.objectstorage.requests.ListObjectsRequest;
import com.oracle.bmc.objectstorage.requests.ListPreauthenticatedRequestsRequest;
import com.oracle.bmc.objectstorage.requests.PutObjectRequest;
import com.oracle.bmc.objectstorage.responses.CreatePreauthenticatedRequestResponse;
import com.oracle.bmc.objectstorage.responses.GetObjectResponse;
import com.oracle.bmc.objectstorage.responses.ListObjectsResponse;
import com.oracle.bmc.objectstorage.responses.ListPreauthenticatedRequestsResponse;
import com.oracle.bmc.objectstorage.transfer.UploadConfiguration;
import com.oracle.bmc.objectstorage.transfer.UploadManager;
import com.oracle.bmc.objectstorage.transfer.UploadManager.UploadRequest;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

public class ObjectStoreRepository implements FileRepository {

	private ObjectStorage objectStorageClient;
	private ObjectStoreConfiguration objectStoreConfiguration;

	public ObjectStoreRepository(ObjectStoreConfiguration objectStoreConfig) {
		this.objectStoreConfiguration = objectStoreConfig;
		Supplier<InputStream> privateKeySupplier = () -> new ByteArrayInputStream(
				objectStoreConfiguration.privateKey().getBytes(StandardCharsets.UTF_8));
		objectStorageClient = new ObjectStorageClient(SimpleAuthenticationDetailsProvider.builder()
				.tenantId(objectStoreConfiguration.getTenantId()).userId(objectStoreConfiguration.getUserId())
				.fingerprint(objectStoreConfiguration.getFingerPrint()).privateKeySupplier(privateKeySupplier)
				.passPhrase(objectStoreConfiguration.getPassphrase()).region(objectStoreConfiguration.getRegion())
				.build());
	}

	@Override
	public void createBucket(String bucketName) throws FileStoreException {
		try {
			CreateBucketDetails createBucketDetails = CreateBucketDetails.builder()
					.compartmentId(objectStoreConfiguration.getCompartmentId()).name(bucketName).build();
			CreateBucketRequest createBucketRequest = CreateBucketRequest.builder()
					.createBucketDetails(createBucketDetails).namespaceName(objectStoreConfiguration.getNameSpace())
					.build();
			objectStorageClient.createBucket(createBucketRequest);
		} catch (Exception ex) {
			throw new FileStoreException(String.format("Failed to create bucket with name \"%s\"", bucketName),
					ex.getCause());
		}
	}

	@Override
	public void deleteBucket(String bucketName) throws FileStoreException {
		try {
			String namespace = objectStoreConfiguration.getNameSpace();
			ListPreauthenticatedRequestsRequest preAuthRequests = ListPreauthenticatedRequestsRequest.builder()
					.bucketName(bucketName).namespaceName(namespace).build();
			ListPreauthenticatedRequestsResponse listPreauthenticatedRequests = objectStorageClient
					.listPreauthenticatedRequests(preAuthRequests);
			while (!listPreauthenticatedRequests.getItems().isEmpty()) {
				listPreauthenticatedRequests.getItems().stream().parallel().forEach(preAuthReq -> {
					DeletePreauthenticatedRequestRequest req = DeletePreauthenticatedRequestRequest.builder()
							.bucketName(bucketName).namespaceName(namespace).parId(preAuthReq.getId()).build();
					objectStorageClient.deletePreauthenticatedRequest(req);
				});
				listPreauthenticatedRequests = objectStorageClient.listPreauthenticatedRequests(preAuthRequests);
			}
			ListObjectsRequest request = ListObjectsRequest.builder().bucketName(bucketName).namespaceName(namespace)
					.build();
			ListObjectsResponse listObjects = objectStorageClient.listObjects(request);
			listObjects.getListObjects().getObjects().stream().parallel().forEach(object -> {
				DeleteObjectRequest delRequest = DeleteObjectRequest.builder().bucketName(bucketName)
						.namespaceName(namespace).objectName(object.getName()).build();
				objectStorageClient.deleteObject(delRequest);
			});
			DeleteBucketRequest DeleteBucketRequest = com.oracle.bmc.objectstorage.requests.DeleteBucketRequest
					.builder().bucketName(bucketName).namespaceName(namespace).build();
			objectStorageClient.deleteBucket(DeleteBucketRequest);
		} catch (Exception ex) {
			throw new FileStoreException(String.format("Failed to delete bucket with name \"%s\"", bucketName),
					ex.getCause());
		}
	}

	@Override
	public boolean doesBucketExist(String bucketName) throws FileStoreException {
		try {
			String namespace = objectStoreConfiguration.getNameSpace();
			HeadBucketRequest headBucketRequest = HeadBucketRequest.builder().bucketName(bucketName)
					.namespaceName(namespace).build();
			objectStorageClient.headBucket(headBucketRequest);
		} catch (Exception ex) {
			throw new FileStoreException(
					String.format("Failed to check bucket exist or bucket does not exist with name \"%s\"", bucketName),
					ex.getCause());
		}
		return true;
	}

	@Override
	public FileTransactionStatus uploadDocument(String bucketName, String objectKey, File file)
			throws FileStoreException {
		try {
			uploadDocument(bucketName, objectKey, new FileInputStream(file));
		} catch (Exception ex) {
			throw new FileStoreException(
					String.format("Failed to upload object as file \"%s\" with key \"%s\" to bucket \"%s\"",
							file.getName(), objectKey, bucketName),
					ex.getCause());
		}
		return FileTransactionStatus.UPLOADED;
	}

	@Override
	public FileTransactionStatus uploadDocument(String bucketName, String objectKey, InputStream inputStream)
			throws FileStoreException {
		try {
			String namespace = objectStoreConfiguration.getNameSpace();
			UploadConfiguration uploadConfiguration = UploadConfiguration.builder().allowMultipartUploads(true)
					.allowParallelUploads(true).build();
			PutObjectRequest putObjectRequest = PutObjectRequest.builder().bucketName(bucketName)
					.namespaceName(namespace).objectName(objectKey).build();
			UploadRequest uploadRequest = UploadRequest.builder(inputStream, inputStream.available())
					.build(putObjectRequest);
			UploadManager uploadManager = new UploadManager(objectStorageClient, uploadConfiguration);
			uploadManager.upload(uploadRequest);
		} catch (Exception ex) {
			throw new FileStoreException(
					String.format("Failed to upload object as input stream with key \"%s\" to bucket \"%s\"", objectKey,
							bucketName),
					ex.getCause());
		}
		return FileTransactionStatus.UPLOADED;
	}

	@Override
	public FileTransactionStatus deleteDocument(String bucketName, String objectKey) throws FileStoreException {
		try {
			String namespace = objectStoreConfiguration.getNameSpace();
			DeleteObjectRequest deleteObjectRequest = DeleteObjectRequest.builder().namespaceName(namespace)
					.bucketName(bucketName).objectName(objectKey).build();
			objectStorageClient.deleteObject(deleteObjectRequest);
		} catch (Exception ex) {
			throw new FileStoreException(
					String.format("Failed to delete object with key \"%s\" from bucket \"%s\"", objectKey, bucketName),
					ex.getCause());
		}
		return FileTransactionStatus.DELETED;
	}

	@Override
	public InputStream downloadDocument(String bucketName, String objectKey) throws FileStoreException {
		try {
			String namespace = objectStoreConfiguration.getNameSpace();
			GetObjectRequest getObjectRequest = GetObjectRequest.builder().namespaceName(namespace)
					.bucketName(bucketName).objectName(objectKey).build();
			GetObjectResponse getObjectResponse = objectStorageClient.getObject(getObjectRequest);
			return getObjectResponse.getInputStream();
		} catch (Exception ex) {
			throw new FileStoreException(
					String.format("Failed to delete object with key \"%s\" from bucket \"%s\"", objectKey, bucketName),
					ex.getCause());
		}
	}

	@Override
	public String generatePreAuthenticatenUploadUrl(String bucketName, String objectKey, Duration duration)
			throws FileStoreException {
		try {
			String baseUrl = objectStoreConfiguration.getBaseUrl();
			String parUri = generatePreAuthenticationUrl(bucketName, objectKey, duration, AccessType.ObjectWrite);
			return new StringBuilder(baseUrl).append(parUri).toString();
		} catch (Exception ex) {
			throw new FileStoreException(String.format(
					"Failed to generate pre-authentication upload url with object key \"%s\" from bucket \"%s\" and duration time \"%s\"",
					objectKey, bucketName, duration.toString()), ex.getCause());
		}
	}

	@Override
	public String generatePreAuthenticatenDownloadUrl(String bucketName, String objectKey, Duration duration)
			throws FileStoreException {
		try {
			String baseUrl = objectStoreConfiguration.getBaseUrl();
			String parUri = generatePreAuthenticationUrl(bucketName, objectKey, duration, AccessType.ObjectRead);
			return new StringBuilder(baseUrl).append(parUri).toString();
		} catch (Exception ex) {
			throw new FileStoreException(String.format(
					"Failed to generate pre-authentication upload url with object key \"%s\" from bucket \"%s\" and duration time \"%s\"",
					objectKey, bucketName, duration.toString()), ex.getCause());
		}
	}

	private String generatePreAuthenticationUrl(String bucketName, String objectKey, Duration duration,
			AccessType accessType) {
		String namespace = objectStoreConfiguration.getNameSpace();
		CreatePreauthenticatedRequestDetails preauthenticatedRequestDetails = CreatePreauthenticatedRequestDetails
				.builder().objectName(objectKey).accessType(accessType).name("PreAuthUrl")
				.timeExpires(DateUtils.addSeconds(new Date(), (int) duration.getSeconds())).build();
		CreatePreauthenticatedRequestRequest preauthenticatedRequestRequest = CreatePreauthenticatedRequestRequest
				.builder().namespaceName(namespace).bucketName(bucketName)
				.createPreauthenticatedRequestDetails(preauthenticatedRequestDetails).build();
		CreatePreauthenticatedRequestResponse preauthenticatedRequestResponse = objectStorageClient
				.createPreauthenticatedRequest(preauthenticatedRequestRequest);
		return preauthenticatedRequestResponse.getPreauthenticatedRequest().getAccessUri();
	}

}
