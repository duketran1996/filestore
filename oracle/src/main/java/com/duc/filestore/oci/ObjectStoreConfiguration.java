package com.duc.filestore.oci;

import com.oracle.bmc.Region;

public interface ObjectStoreConfiguration {

    String getTenantId();

    String getUserId();

    String getFingerPrint();

    String privateKey();
    
    String getPassphrase();

    Region getRegion();

    String getCompartmentId();

    String getNameSpace();
    
    String getBaseUrl();

}
